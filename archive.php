<?php get_template_part('templates/html', 'header'); ?>


<div class="section hero section-wrapper">
  <div class="main-container">
    <div class="header-interna">
      <div class="hero__content hero__interna">
        <h1 class="title-heading">Blog</h1>
        <p class="text-large center">Confira todas as nossas postagens</p>
      </div>
    </div>
    <div class="bg shape-2"></div>
    <div class="bg shape-1"></div>
  </div>
</div>
<div class="section section-cursos">
  <div class="main-container main-container-small">
    <div id="blo" class="blog-grid">

    <?php include(locate_template('templates/content/loop-post.php')); ?>

    </div>
  </div>
  <div class="divider w98"></div>
</div>

<?php get_template_part('templates/html', 'footer'); ?>