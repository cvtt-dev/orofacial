
   <?php /* Template Name: Página - sobre */ ?>
   <?php get_template_part('templates/html','header'); ?>

   <?php while (have_posts()) : the_post(); ?>



<section class="section top-section">

    <div class="wrapper w-container">

        <div class="politicas-texto">

            <?php the_content(); ?>                    

        </div>

    </div>

</section>

<?php endwhile; ?>

  <div class="section hero section-wrapper">
    <div class="main-container">
      <div class="header-interna">
        <div class="hero__content hero__interna">
          <h1 class="title-heading">Sobre</h1>
          <p class="text-large center">Uma escola de cursos livres, criada para torna-lo mais criativo, subversivo, sensível e do bem.</p>
        </div>
      </div>
      <div class="bg shape-2"></div>
      <div class="bg shape-1"></div>
    </div>
  </div>
  <div class="section section-cursos">
    <div class="main-container main-container-small"></div>
    <div class="divider w98"></div>
  </div>
<?php get_template_part('templates/html','footer'); ?>
   