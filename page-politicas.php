<?php /* Template Name: Página Políticas */ ?>
<?php get_template_part('templates/html','header'); ?>

<?php while (have_posts()) : the_post(); ?>

<section class="section top-section">
    <div class="wrapper w-container">
        <div class="politicas-texto">
            <?php the_content(); ?>                    
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php get_template_part('templates/html','footer');?>