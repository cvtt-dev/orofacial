<?php //if( !isset($_GET['_sf_s'])) { 
    //$site = get_bloginfo('url'); 
    //wp_redirect($site.'/404.php'); exit; 
    //} ?>

<?php get_template_part('templates/html','header'); ?>

<section class="main-container section-search-cursos">

    <div class="wrapper">

        <div class="infos-busca">
            <?php if(isset($_GET['s'])): ?>
                <h4 class="infos-busca__back-link"><a href="<?php echo get_post_type_archive_link('cursos'); ?>" title="Voltar para Escolas Médicas">Voltar para Cursos</a></h4>
            <?php endif; ?>
            <h3 class="infos-busca__title">Resultado da busca</h3>

            <h2 class="infos-busca__text">

                <?php  

                    global $wp_query;
                    $totalpost = $wp_query->found_posts;  

                    if($totalpost >= 1) { 

                        echo $_GET['_sf_s'] ?? $_GET['s']; 

                    } else { 

                       echo 'Nada Encontrado';

                    } 

                ?> 

            </h2>

            <div class="infos-banner__icone"></div>

        </div>

    </div>

</section>


<section class="main-container main-container-small">

    <div class="wrapper">

        <?php 
            if(isset($_GET['s']) && isset($_GET['post_type'])) :
            $type = $_GET['post_type']; 
        
                if($type == 'cursos'):

                    $args = array(  
                        'post_type'      => $type,
                        's'              => get_search_query(),
                        'post_status'    => 'publish',
                        'posts_per_page' => -1, 
                        'orderby'        => 'title', 
                        'order'          => 'ASC', 
                    );
                
                    $escolas = new WP_Query( $args ); 
                    if($escolas->have_posts()): ?>
                        <div class="w-layout-grid image-link-box-grid image-link-split" id="show-cursos">
                            <?php while ( $escolas->have_posts() ) : $escolas->the_post();
                                include(locate_template('templates/content/loop-search-curso.php')); 
                            endwhile; wp_reset_postdata(); ?>
                        </div>
                    <?php else:
                        include(locate_template('templates/content/404.php'));
                    endif; 
                endif; ?>
            
            </div>

        <?php elseif( $_GET['_sf_s'] ): ?>

            <?php if(have_posts()): ?>
                
                <div class="infos">

                    <?php while (have_posts()) : the_post(); ?>

                        <?php include(locate_template('templates/content/loop-search.php')); ?>

                    <?php endwhile; ?>
                
                </div>

            <?php else: ?>
                
                <?php include(locate_template('templates/content/404.php')); ?>
            
            <?php endif; ?>

        <?php else: ?>
            
            <?php include(locate_template('templates/content/404.php')); ?>

        <?php endif; ?>

    </div>

</section>

<?php get_template_part('templates/html','footer');?>