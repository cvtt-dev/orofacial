<?php get_template_part('templates/html','header');?>

<div class="section hero section-wrapper">
  <div class="main-container">
    <div class="header-interna">
      <div class="hero__content hero__interna">
        <h1 class="title-heading">Eventos</h1>
        <p class="text-large center">Uma escola de cursos livres, criada para torna-lo mais criativo, subversivo, sensível e do bem.</p>
      </div>
    </div>
    <div class="bg shape-2"></div>
    <div class="bg shape-1"></div>
  </div>
</div>
<div class="section section-cursos">
  <div class="main-container main-container-small">
    <div class="section-title center">
      <h3 class="large-heading">Maio/2021</h3>
    </div>
    <div class="w-layout-grid image-link-box-one-line">

  <?php     
  
    while (have_posts()) : the_post(); $loop[] = get_the_ID();
    $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
    $eday = get_post_meta(get_the_ID(), 'component_evento_eday', true);
    $nday = get_post_meta(get_the_ID(), 'component_evento_nday', true);
    $month = get_post_meta(get_the_ID(), 'component_evento_month', true);
    $year = get_post_meta(get_the_ID(), 'component_evento_year', true);
    $hour = get_post_meta(get_the_ID(), 'component_evento_hour', true);
    
  ?>



    <div class="container-line">
      <a href="<?php the_permalink();?>" class="image-link-box image-link-box-line w-inline-block"><img src="<?php if($thumbnail_url): echo $thumbnail_url; else: echo ""; endif; ?>" alt="" class="image-event">
        <div class="boxed boxed-event">
          <div class="category-event"><img src="images/icon-calendar-dark.svg" loading="lazy" alt="" class="image-4">
            <div class="category-text-event"><?php echo $eday; ?>, <?php echo $nday; ?> de <?php echo $month; ?> de <?php echo $year; ?>, às <?php echo $hour; ?> horas</div>
          </div>
          <div class="image-link-box-content">
            <div class="titlte-event"><?php echo the_title(); ?></div>
            <div class="description">
              <p class="paragraph"><?php echo the_content(); ?></p>
            </div>
          </div>
        </div>
      </a>
    </div>

    </div>

    <?php endwhile; wp_reset_postdata();?>


  </div>
  
  <div class="main-container main-container-small">
    <!-- <div class="section-title center">
      <h3 class="large-heading">Abril/2021</h3>
    </div> -->
    <div class="w-layout-grid image-link-box-one-line">

    </div>
    <a href="http://mo.na/sign-up" target="_blank" class="hero__btn max__350 margin_btn w-button">Ver todos os eventos</a>
  </div>
  <div class="divider w98"></div>
</div>

  

  <?php get_template_part('templates/frontpage','alunos');?>
  <?php get_template_part('templates/frontpage','destaque');?>
  <?php get_template_part('templates/html','footer');?>