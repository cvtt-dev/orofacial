<?php $settings = get_option('options_gerais'); ?>

<div class="section">
    <div class="main-container main-container-cursos-modalidade">
      <div class="section-title center">
        <h3 class="large-heading"><?php if($settings['modalidades_home_title']):
           echo $settings['modalidades_home_title']; else: echo ""; endif?></h3>
        <div class="text-medium"><?php if($settings['modalidades_home_title']):
           echo $settings['modalidades_home_sub']; else: echo ""; endif?></div>
      </div>


      <div data-animation="slide" data-hide-arrows="1" data-duration="500" class="category-slider categories w-slider">
        <div class="slider-mask w-slider-mask">

        <?php

          $terms = get_terms(array(
            'taxonomy' =>'modalidade',
            'hide_empty' => false
          ));

          foreach($terms as $term):
            $thumb = get_term_meta( $term->term_id, 'term_modalidade_thumb', true);
            $color = get_term_meta( $term->term_id, 'term_modalidade_thumb_color', true);
            $img = wp_get_attachment_image_src($thumb, 'medium_large'); 


          ?>

          <div class="slider-categories-slide w-slide">
            <div class="container-small align-center container-full">
              <div class="boxed icon-box image-link-box">
                <div class="bg-icon" style="background:<?php echo $color?>;"><img src="<?php echo $img[0]; ?>" alt="" class="icon-large icon-blue"></div>
                <div class="content-icon">
                  <h5 class="title-text-2"><?php echo $term -> name; ?></h5>
                  <div class="text-small"><?php echo $term -> description; ?></div>
                </div>
                <a href="<?php echo get_site_url(); ?>/cursos" class="button bg-blue hover-box-button top-space w-inline-block">
                  <div>Explorar cursos</div>
                </a>
              </div>
            </div>
          </div>

          <?php endforeach; ?>
          
        
        </div>
        <div class="slider-icon left w-slider-arrow-left">
          <div class="icon-3 w-icon-slider-left"></div>
        </div>
        <div class="slider-icon w-slider-arrow-right">
          <div class="icon-2 w-icon-slider-right"></div>
        </div>
      </div>
    </div>
  </div>