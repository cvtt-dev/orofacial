<?php $settings = get_option('options_gerais'); ?>

<div class="section">
    <div class="divider w98"></div>
    <div class="main-container main-container-top-small">
      <div class="section-title center">
        <h3 class="large-heading"><?php if($settings['blog_home_title']): echo $settings['blog_home_title'] ?></h3>
        <?php else: echo ""; endif; ?>
        <div class="text-medium"><?php if($settings['blog_home_sub']): echo $settings['blog_home_sub'] ?>
        <?php else: echo ""; endif; ?></div>
      </div>
      <div class="w-layout-grid image-link-box-grid">
        <?php get_template_part('templates/content/loop','blog');?>
      </div>
      <a href="<?php if($settings['blog_home_link']): echo $settings['blog_home_link']; else: echo ""; endif; ?>" style="background:<?= $settings['blog_home_cta_color']; ?>"target="_blank" class="hero__btn max__350 margin_btn w-button">
      <?php if($settings['blog_home_cta']): echo $settings['blog_home_cta']; else: echo ""; endif; ?>
      </a>
    </div>
  </div>