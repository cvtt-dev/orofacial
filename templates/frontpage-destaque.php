<?php $settings = get_option('options_gerais'); ?>

<div class="section bg-orange">
    <div class="main-container no-space wrapper cta-wrapper-2">
      <div class="w-layout-grid grid-halves fullwidth-grid-halves">
        <div class="container-grid align-center z999">
          <h1 class="display-heading-2 text-white text-blue"><?php if($settings['aulas_home_title']): echo $settings['aulas_home_title'] ?>
          <?php else: echo ""; endif; ?>
          </h1>
          <p class="text-large-2 text-blue"><?php if($settings['aulas_home_sub']): echo $settings['aulas_home_sub'] ?>
          <?php else: echo ""; endif; ?>
          </p>
          <a href="<?php echo get_post_type_archive_link('cursos'); ?>" style="background:<?= $settings['aulas_home_cta_color']; ?>" target="_blank" class="hero__btn max_100 footer__btn w-button">
          <?php if($settings['aulas_home_cta']): echo $settings['aulas_home_cta']; else: echo ""; endif; ?>
          </a>
        </div>
        
          <?php if($settings['aulas_home_img']): foreach($settings['aulas_home_img'] as $img):?>
          <?php $url = wp_get_attachment_url($img, 'full', '', array('class' => '')); ?>
          <?php endforeach;?>
        <div data-w-id="5101fcdb-8a65-7d0c-d5bf-d066e4d5986f" class="image-with-caption-wrapper">

          <img src="<?= $url; ?>" width="684" sizes="(max-width: 479px) 87vw, (max-width: 767px) 400px, (max-width: 991px) 600px, 684px" alt="" class="rounded-large cta-image">
          <!-- <div class="alert image-caption">
            <div class="small-text">Aulas 100% online</div>
          </div>
          <div class="element-overlay-top-left">
            <div class="icon-badge bg-primary-4 shadow-large">
              <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/pc-icons.svg" alt="" class="image"></div>
          </div>
          <div class="element-overlay-bottom-right">
            <div class="icon-badge shadow-large">
              <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/icon2.png" alt="">
            </div>
          </div> -->
        </div>
        <?php endif; ?>
      </div>
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/circle2.svg" loading="lazy" alt="" class="circle _2">
    </div>
</div>