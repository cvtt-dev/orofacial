<?php $settings = get_option('options_gerais'); ?>

<div class="section">
    <div class="main-container main-container-small">
      <div class="container-large align-center section-title">
        <h4 class="large-heading">
          <?php if($settings['alunos_home_title']): echo $settings['alunos_home_title']; ?>
          <?php else: echo ""; endif; ?></h4>
      </div>
      <div class="w-layout-grid quotes-grid-thirds">

      <?php if($settings['alunos_home_blocos']): foreach($settings['alunos_home_blocos'] as $bloco): ?>
        <?php foreach($bloco['alunos_home_blocos_icon'] as $img): ?>
          <?php $url = wp_get_attachment_url($img, "full", '',array('class'=>''));?>
        <?php endforeach; ?>
        <div class="boxed small-quote-box shadow"><img src="<?php if($url): echo $url; else: echo ""; endif; ?>" alt="" class="avatar avatar-small quote-box-avatar">
          <div class="text-depoimentos">“<?php echo $bloco['alunos_home_blocos_desc']; ?>”</div>
          <div class="text-small-2 quote-author"><?php echo $bloco['alunos_home_blocos_name']; ?>, <?php echo $bloco['alunos_home_blocos_region']; ?></div>
        </div>
      <?php endforeach; endif; ?>
      </div>
    </div>
  </div>