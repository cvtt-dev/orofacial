<?php 
    $thumb_url  = get_the_post_thumbnail_url(get_the_ID(), 'thumb');
    $tipo       = get_the_terms(get_the_ID(), 'tipo' );
    $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
    $hour = get_post_meta(get_the_ID(), 'component_cursos_hour', true);
    $day = get_post_meta(get_the_ID(), 'component_cursos_day', true);
    $price = get_post_meta(get_the_ID(), 'component_cursos_price', true);
    $new_price = get_post_meta(get_the_ID(), 'component_cursos_price_new', true);
?>

<div id="post-<?php the_ID(); ?>" class="mfcc-component-blog-postagens container-small align-center">
    <a href="<?php the_permalink();?>" class="image-link-box w-inline-block"><img src="<?php if($thumb_url): echo $thumb_url; else: echo ""; endif; ?>" sizes="(max-width: 479px) 91vw, 400px" alt="" class="image-course">
    <div class="boxed square-top boxed-small">
        <div class="image-link-box-content">
        <div class="title-text"><?php echo mb_strimwidth(the_title(),0,50,'...'); ?></div>
        <div class="description">
            <p class="paragraph"><?php echo mb_strimwidth(the_content(),0,50,'...'); ?></p>
        </div>
        <div class="divider"></div>
        <div class="details">
            <div class="category card-course-curso__div"><img src="<?php echo get_template_directory_uri();?>/assets/images/icon-academy.png" loading="lazy" alt="" class="icon saturation">
            <div class="category-text"><?= $hour ?>h / <?= $day ?> dias</div>
            </div>
            <div class="price">
            <div class="price-text"><span class="mkt-price">R$<?= $price ?></span> R$<?= $new_price ?></div>
            </div>
        </div>
        </div>
    </div>
    </a>
</div>