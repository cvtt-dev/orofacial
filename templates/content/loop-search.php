<?php 

    $thumbnail_url    = get_the_post_thumbnail_url(get_the_ID(), 'thumb'); 

    $cat              = get_the_category(get_the_ID());

    $day              = get_the_date('d', get_the_ID());   

    $month            = get_the_date('F', get_the_ID());   

    $data             = $day.' de '.$month;

    $color            = get_term_meta( $cat[0]->term_id, 'term_color', true);

?>

<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="post-card-v2-big w-inline-block slider-card">

    <article class="post-card-content">

        <div class="badge" style="background:<?php echo $color; ?>;"><?php echo $cat[0]->name; ?></div>

        <h3 class="post-v2-heading"><?php the_title(); ?></h3>

        <div class="post-info-2 text-white">

            <div class="post-info-author text-white slider-info">

                <img src="https://uploads-ssl.webflow.com/5fa443314944220d73966316/5fa443310cae07fbe2288da7_user-white.svg" alt="" class="mini-icon slider-icon">

                <div><?php the_author(); ?></div>

                <div class="divider-small transparent"></div>

            </div>

            <div class="post-info-block-2 slider-info">

                <img src="https://uploads-ssl.webflow.com/5fa443314944220d73966316/5fa443310cae07d071288d94_calendar-white.svg" alt="" class="mini-icon slider-icon">

                <div><?php echo $data; ?></div>

            </div>

            <div class="post-info-block-2 slider-info">

                <div class="divider-small transparent"></div>

                <img src="https://uploads-ssl.webflow.com/5fa443314944220d73966316/5fa443310cae073d8e288d91_clock-white.svg" alt="" class="mini-icon slider-icon">

                <div><?php echo do_shortcode('[rt_reading_time label="" postfix="minutos de leitura" postfix_singular="minutos de leitura"]'); ?></div>

            </div>

        </div>

    </article>

    <div class="post-gradient"></div>

    <div class="thumbnail-2" style="background-image: url('<?php echo $thumbnail_url;?>'); background-position: center; filter: brightness(99.8242%); transform: translate3d(0px, 0px, 0px) scale3d(1.00105, 1.00105, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d; will-change: filter, transform;"></div>

</a>