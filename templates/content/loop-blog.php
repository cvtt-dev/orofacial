<?php 

    $args = array('post_type' => 'post', 'posts_per_page' => 3, 'orderby' => 'date', 'order' => 'DESC' );
    $loop = new WP_Query($args); 
    while ($loop->have_posts()) : $loop->the_post(); 
    $getCat = get_the_category();
    $day    = get_the_date('d', get_the_ID());   
    $month  = get_the_date('m', get_the_ID());
    $year  = get_the_date('y', get_the_ID());
    $data   = $day.'/'.$month.'/'.$year;
    $cat   = get_the_category(get_the_ID());
    $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');

?>
<div class="container-small align-center">
    <a href="<?php the_permalink(); ?>" class="image-link-box w-inline-block"><img src="<?php echo $thumbnail_url;?>" sizes="(max-width: 479px) 91vw, 400px" alt="" class="image-course">
    <div class="boxed square-top boxed-small">
        <div class="blog-details">
        <div class="badge">
            <div><?php echo $cat[0]->name; ?></div>
        </div>
        <div>
            <div>Publicado em <?php echo $data; ?></div>
        </div>
        </div>
        <div class="image-link-box-content">
        <div class="title-text"><?php echo mb_strimwidth(get_the_content(),0,120,'...'); ?></div>
        </div>
    </div>
    </a>
</div>
<?php endwhile; wp_reset_postdata();?>

