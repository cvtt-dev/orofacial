<?php 

$args = array('post_type' => 'post', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC' );
$loop = new WP_Query($args); 
while ($loop->have_posts()) : $loop->the_post(); 
$getCat = get_the_category();
$day    = get_the_date('d', get_the_ID());   
$month  = get_the_date('m', get_the_ID());
$year  = get_the_date('y', get_the_ID());
$data   = $day.'/'.$month.'/'.$year;
$cat   = get_the_category(get_the_ID());
$thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');

?>


<a item="permalink" href="<?php the_permalink(); ?>" class="link-block w-inline-block">

<img src="<?= $thumbnail_url; ?>" sizes="(max-width: 479px) 91vw, 400px" alt="" class="image-course" >
    <div>
    <div class="data">Publicado em <?= $day ?> de <?= $month ?> de <?= $year ?></div>
    </div>
    <div>
    <h1 item="title" class="heading heading-link"><?php the_title(); ?></h1>
    </div>
    <div>
    <div item="date" class="text-small text-small-blog section-blog-card__sub"><?php echo mb_strimwidth(get_the_content(),0,200,'...'); ?></div>
    </div>
    <div>
    <div class="button button-leia-mais">
        <p class="button-text">LEIA MAIS</p>
    </div>
    </div>
</a>

<?php endwhile; ?>