<?php 
    // $cat   = get_the_category(get_the_ID()); 
    // $tipo  = get_the_terms(get_the_ID(), 'tipo' ); // pega a categoria da taxonomia tipo.
    $thumb = get_the_post_thumbnail_url(get_the_ID(), 'thumb');
    // $thumb_default = get_template_directory_uri().'/assets/images/not-defined.jpg';
    $link          = get_the_permalink();
    $title         = get_the_title();
    $content       = get_the_content();
    $hour = get_post_meta(get_the_ID(), 'component_cursos_hour', true);
    $day = get_post_meta(get_the_ID(), 'component_cursos_day', true);
    $price = get_post_meta(get_the_ID(), 'component_cursos_price', true);
    $new_price = get_post_meta(get_the_ID(), 'component_cursos_price_new', true);
    $dir = get_template_directory_uri();
?>


<?php
    
    $html_postagem .= '<div id="'.$link.'" class="mfcc-component-blog-postagens container-small align-center">
        <a href="'.$link.'" class="image-link-box w-inline-block"><img src="'.$thumb.'" sizes="(max-width: 479px) 91vw, 400px" alt="" class="image-course">
        <div class="boxed square-top boxed-small">
            <div class="image-link-box-content">
            <div class="title-text">'.$title.'</div>
            <div class="description">
                <p class="paragraph">'.$content.'</p>
            </div>
            <div class="divider"></div>
            <div class="details">
                <div class="category card-course-curso__div"><img src="'.$dir.'/assets/images/icon-academy.png" alt="" class="icon saturation">
                <div class="category-text">'.$hour.'h / '.$day.' dias</div>
                </div>
                <div class="price">
                <div class="price-text"><span class="mkt-price">R$'.$price.'</span> R$'.$new_price.'</div>
                </div>
            </div>
            </div>
        </div>
        </a>
    </div>';
    
?>