<?php get_template_part('templates/html', 'head'); ?>

<div class="section-topo">
    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="nav w-nav">
      <div class="nav__container">
        
          <a href="<?php echo get_site_url(); ?>" class="brand w-nav-brand header-img">
              <?= create_responsive_image(get_theme_mod( 'logo_header_desktop'));?>
          </a>

        <?php if(!wp_is_mobile()): ?>

        <nav role="navigation" class="nav__menu w-nav-menu">
          <?php wp_nav_menu(array('theme_location' => 'menu_1', 'menu_class' => 'nav__menu w-nav-menu')); ?>
          <a href="#" class="btn w-button section-top_botom">Login</a>
        </nav>

        <?php endif; ?>

        <?php if(wp_is_mobile()): ?>

            <button type="button" class="component-header__toggle hamburger hamburger--spring" id="menu-header-mobile-button">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
            
        <?php endif; ?>

      </div>

       <?php if(wp_is_mobile()): ?>

            <nav class="component-header__nav" id="menu-header-mobile">
                <?php wp_nav_menu(array('theme_location' => 'menu_3', 'menu_class' => 'component-menu-mobile')); ?>
            </nav>

       <?php endif; ?>

    </div>
        
    
</div>
