<?php $settings = get_option('options_gerais'); ?>

<div class="section section-cursos">
    <div class="main-container main-container-small">
      <div class="section-title center">
        <h3 class="large-heading"><?php if($settings['cursos_home_title']): echo $settings['cursos_home_title']; else: echo ""; endif; ?></h3>
        <div class="text-medium"><?php if($settings['cursos_home_sub']): echo $settings['cursos_home_sub']; else: echo ""; endif; ?></div>
      </div>

      <div class="w-layout-grid image-link-box-grid">
      <?php     

      $args = array('post_type' => 'cursos', 'meta_key' => 'cursos_destaque_option', 'meta_value' => '1','posts_per_page' => 6, 'orderby' => 'date', 'order' => 'DESC');

      $destaque = new WP_Query($args);

      while ($destaque->have_posts()) : $destaque->the_post(); 
      $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
      $hour = get_post_meta(get_the_ID(), 'component_cursos_hour', true);
      $day = get_post_meta(get_the_ID(), 'component_cursos_day', true);
      $price = get_post_meta(get_the_ID(), 'component_cursos_price', true);
      $new_price = get_post_meta(get_the_ID(), 'component_cursos_price_new', true);
      $color = get_post_meta(get_the_ID(), 'cursos_home_cta_color', true);
    ?>

      
        <div class="container-small align-center">
          <a href="<?php the_permalink();?>" class="image-link-box w-inline-block"><img src="<?php if($thumbnail_url): echo $thumbnail_url; else: echo ""; endif; ?>" sizes="(max-width: 479px) 91vw, 400px"  alt="" class="image-course">
            <div class="boxed square-top boxed-small">
              <div class="image-link-box-content">
                <div class="title-text"><?php echo mb_strimwidth(the_title(),0,50,'...'); ?></div>
                <div class="description">
                  <p class="paragraph"><?php echo mb_strimwidth(the_content(),0,50,'...'); ?></p>
                </div>
                <div class="divider"></div>
                <div class="details">
                  <div class="category"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-academy.png" loading="lazy" alt="" class="icon saturation">
                    <div class="category-text"><?= $hour ?>h / <?= $day ?> dias</div>
                  </div>
                  <div class="price">
                    <div class="price-text"><span class="mkt-price">R$<?= $price ?></span> R$<?= $new_price ?></div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        
      <?php endwhile; wp_reset_postdata();?>

      </div>
      <a href="<?php echo get_post_type_archive_link('cursos'); ?>" target="_blank" style="background:<?= $color; ?>" class="hero__btn max__350 margin_btn w-button"><?php echo $settings['cursos_home_cta']; ?></a>
    </div>
    <div class="divider w98"></div>
  </div>