<?php $settings = get_option('options_gerais'); ?>

<div class="section hero hero-home">
    <div class="main-container">
      <div class="hero__grid">
        <div class="hero__content">
          <h1 class="title-heading section-hero-txt"><?php if($settings['banner_home_title']): echo $settings['banner_home_title']; else: echo "" ; endif; ?></h1>
          <p class="text-large space-bottom"><?php if($settings['banner_home_sub']): echo $settings['banner_home_sub']; else: echo "" ; endif; ?></p>
          <div class="div-block">
              
            <?php foreach($settings['banner_home_itens'] as $btn): ?>

                <a href="<?php if($btn['banner_home_cta_link']): echo $btn['banner_home_cta_link']; else: echo "" ; endif; ?>" style="background:<?php echo $btn['banner_home_cta_color'];?>; margin-left:10px;" target="_blank" class="hero__btn w-button">
                <?php if($btn['banner_home_cta_name']): echo $btn['banner_home_cta_name']; else: echo "" ; endif; ?></a>

            <?php endforeach; ?>

          </div>
        </div>
        <div id="w-node-fac2ad7d-b342-893a-6088-3957d5aa2d32-c173ce03" class="hero__img">

            <?php if($settings['banner_home_img']): ?>

            <div class="image-with-caption-wrapper">
                <?php foreach($settings['banner_home_img'] as $img ): ?>
                    <?php $url = wp_get_attachment_image_url($img,'full','',array('class'=>'rounded-left shadow-large image-topo'));?>
                <?php endforeach;?>
                    <img src="<?php echo $url; ?>" width="684" data-w-id="8a821d31-4b15-1d23-1488-9bf1ddb5af05" alt="" class="rounded-large cta-image topo-image">
                    
                    <!-- <div class="element-overlay-top-left custom-top-left">
                        <div data-w-id="8a821d31-4b15-1d23-1488-9bf1ddb5af0a" class="icon-badge bg-primary-4 shadow-large icon-gray">
                            <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/icon1.png" alt="" class="image image-2 invert">
                        </div>
                    </div>
                    <div class="element-overlay-bottom-right custom-top-right">
                        <div data-w-id="8a821d31-4b15-1d23-1488-9bf1ddb5af0d" class="icon-badge shadow-large icon-orange">
                            <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/icon2.png" alt="">
                        </div>
                    </div> -->
            </div>

            <?php endif; ?>
        </div>
      </div>
      
      <div class="bg shape-2"></div>
      <div class="bg shape-1"></div>
    </div>
        <div class="bg square-1"></div>
    </div>
</div>