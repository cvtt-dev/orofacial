<?php $settings = get_option('options_gerais'); ?>

<div class="section bg-blue">
    <div class="main-container no-space wrapper wrapper-small">
      <div class="w-layout-grid grid-halves fullwidth-grid-halves">
        <div>
          <div data-w-id="7a5fe882-7eed-7ad0-96d4-944d2d2076a9" class="image-wrapper media-div"><img src="<?php echo get_template_directory_uri(); ?>/_src/images/60a662a24a981dbd53efa352_pexels-cedric-fauntleroy-4269264.jpg" loading="lazy"  alt="" class="ipad-image">
            <div class="iphone-video">

            <?php foreach($settings['apreendizado_home_img'] as $icon): ?>
              <?php $url = wp_get_attachment_image_url($icon,'full','',array('class'=>''));?>
            <?php endforeach; ?>
              
              <div data-poster-url="<?php echo $url ?>" data-autoplay="true" data-loop="true" data-wf-ignore="true" class="background-video w-background-video w-background-video-atom">
              <?php foreach($settings['apreendizado_home_gif'] as $gif): ?>
                <?php $animation = wp_get_attachment_image_url($gif,'full','',array('class'=>''));?>
              <?php endforeach; ?>
              <video autoplay="" loop="" style="background-image:url(&quot;<?php echo $animation; ?>&quot;)" muted="" playsinline="" data-wf-ignore="true" data-object-fit="cover">
                  <source src="<?php echo $animation; ?>" data-wf-ignore="true">
                  <source src="<?php echo $animation; ?>" data-wf-ignore="true">
                </video></div>
            </div>
          </div>
        </div>
        <div id="w-node-_64e95df9-033f-1724-4b9c-6837b15dd6a1-c173ce03" class="container-grid align-center z999">
          <h1 class="display-heading-2 text-white">
            <?php if($settings['aprendizado_home_title']): ?> 
            <?php echo $settings['aprendizado_home_title']; ?> 
            <?php else: echo "";?>
             <?php endif; ?> </h1>
          <p class="text-large-2">
          <?php if($settings['aprendizado_home_sub']): ?> 
            <?php echo $settings['aprendizado_home_sub']; ?> 
            <?php else: echo ""; ?>
            <?php endif; ?>
          </p>
          <a href="<?php echo get_post_type_archive_link('cursos'); ?>" target="_blank" style="background:<?php echo $settings['aprendizado_home_color']; ?>" class="hero__btn max_100 bg-orange top-space w-button">
          <?php if($settings['aprendizado_home_cta']): echo $settings['aprendizado_home_cta']; else: echo ""; endif; ?></a>
        </div>
      </div>
      
        <img src="<?php echo get_template_directory_uri(); ?>/_src/images/circle4.svg" loading="lazy" alt="" class="circle _4">
       
        <img src="<?php echo get_template_directory_uri(); ?>/_src/images/circle3.svg" loading="lazy" alt="" class="circle _3">
       
        <img src="<?php echo get_template_directory_uri(); ?>/_src/images/circle2.svg" loading="lazy" alt="" class="circle _2">
        
        <img src="<?php echo get_template_directory_uri(); ?>/_src/images/circle1.svg" loading="lazy" alt="" class="circle _1">
    </div>
  </div>
</div>