<?php $settings = get_option('options_gerais'); ?>

</main>

<div class="footer-section">
    <div class="main-container-2">
      <div class="footer-3-top-row">
        <div class="footer-3-logo-and-menus">
          <a href="<?php echo get_site_url(); ?>" class="footer-3-logo w-inline-block">
           <?= create_responsive_image(get_theme_mod( 'logo_footer_desktop'));?>
          </a>
          <div class="w-layout-grid menu-grid-vertical">
            <?php wp_nav_menu(array('theme_location' => 'menu_2', 'menu_class' => 'w-layout-grid menu-grid-vertical')); ?>
          </div>
        </div>
        <div class="container-2">
          <h5 class="footer-form-heading">Receba nossas novidades por email:</h5>
          <div class="form-block w-form">
            <form class="form-grid-vertical">
                <?php echo do_shortcode( '[contact-form-7 id="5" title="Formulário de contato 1"]' ); ?>
              </form>
            <div class="form-success w-form-done">
              <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="form-error w-form-fail">
              <div>Oops! Something went wrong while submitting the form.</div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom-row footer-3-bottom-row main-container-2">
        <div class="footer-bottom-links">
          <a href="#" class="row-link text-white">Política de Privacidade</a>
          <div>Site criado por <a href="https://www.webflow.com/" target="_blank" class="hover-link text-white">Visualpage</a>
          </div>
        </div>
        <div class="social-links">
          <?php if($settings['footer_home_sociais_bloco']): foreach($settings['footer_home_sociais_bloco'] as $bloco): ?>
            <?php foreach($bloco['footer_home_social_img'] as $img): ?>
              <?php $url = wp_get_attachment_url($img, "full", '',array('class'=>''));?>
            <?php endforeach; ?>

          <a href="<?= $bloco['footer_home_social_link']; ?>" target="_blank" class="social-link hover-link w-inline-block">
          <img src="<?= $url; ?>" alt="" class="social-link-image"></a>
         
          <?php endforeach; endif; ?>
        </div>
      </div>
    </div>
  </div>

<a href="javascript:void(0);" id="scroll" class="fs-component-scroll" title="Scroll to Top" style="display: none;">Top<span></span></a>

<?php wp_footer(); ?>
</body>
</html>