
<?php get_template_part("templates/html", "header") ?>

<?php

    while ( have_posts() ) : the_post(); 
    $thumb = get_the_post_thumbnail_url(get_the_ID(), 'large');
    $day    = get_the_date('d', get_the_ID());   
    $month  = get_the_date('m', get_the_ID());
    $year  = get_the_date('y', get_the_ID());
    $cat   = get_the_category(get_the_ID());

?>
<div class="section hero section-wrapper">
    <div class="main-container">
      <div class="header-interna">
        <div class="hero__content hero__interna">
          <h1 class="title-heading"><?= the_title(); ?></h1>
          <p class="text-large center"><?= $cat[0]->name; ?> | Publicado em <?= $day; ?> de <?= $month; ?> de <?= $year; ?></p>
        </div>
      </div>
      <div class="bg shape-2"></div>
      <div class="bg shape-1"></div>
    </div>
  </div>
  <div class="section section-cursos single-curso-content">
    <div class="main-container main-container-small">
      <div>
        <!-- <h1 class="heading-2"><?php //the_title(); ?></h1> -->
      </div>
      <div><img src="<?php if($thumb): echo $thumb; else: echo get_template_directory_uri().'/assets/images/blog-thumb.png'; endif; ?>" loading="lazy" sizes="(max-width: 479px) 92vw, (max-width: 767px) 94vw, (max-width: 1919px) 95vw, 1248px" alt="" class="image-5"></div>
      <div>
        <div class="w-richtext">
          <?= get_the_content(); ?>
        </div>
      </div>
    </div>
</div>

<?php endwhile; ?>

<div class="section section-cursos">
    <div class="main-container main-container-small main-veja-tbm">
      <div class="divider w98"></div>
      <h1 class="veja-tbm">Veja também</h1>
      <div id="blo" class="blog-grid">
          <?php get_template_part("templates/content/loop", "blog") ?>
      </div>
    </div>
</div>

<?php get_template_part("templates/html", "footer") ?>