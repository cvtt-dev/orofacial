<?php

/*

* Configurações de Taxonomias

* Desenvolvedor: Bruno Lima

*/

$labels = array( 'name' => _x( 'Modalidade', 'Modalidade', 'text_domain' ));

$args   = array(

  'labels'            => $labels,

  'hierarchical'      => true,

  'public'            => true,

  'show_ui'           => true,

  'show_admin_column' => true,

  'show_in_nav_menus' => true,

  'show_tagcloud'     => true,

  'rewrite'           => array('slug' => 'modalidade'),

);



register_taxonomy( 'modalidade', array( 'cursos' ), $args);