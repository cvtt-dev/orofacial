<?php

/*

* AJAX functions

*/


function getPostagem() {

    $cat     = $_POST['categoria'];
    $name    = $_POST['name'];
    
    if ( $cat == 0 ):
        echo 'Nenhuma postagem encontrada';
    else : 
        $args  = array('post_type' => array('cursos'), 'tax_query' => array( array( 'taxonomy' => 'modalidade', 'field' => 'term_id', 'terms' => $cat, ), ), 'posts_per_page' => 3, 'orderby' => 'title', 'order' => 'DESC');
    endif;
    
    // esse loop é para quando fizer uma requisição na página.
    $postagem = new WP_Query($args);

    $html_postagem = '';
    
    while ($postagem->have_posts()) : $postagem->the_post();
        include(locate_template('templates/content/loop-curso.php'));  
    endwhile; wp_reset_postdata();


    echo json_encode( array( 'html_postagem' => array($html_postagem),));

    exit;
 }

add_action('wp_ajax_getPost', 'getPostagem');
add_action('wp_ajax_nopriv_getPost', 'getPostagem');