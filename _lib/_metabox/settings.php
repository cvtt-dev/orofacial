<?php

/*

* Metabox Settings

* Desenvolvedor: Bruno Lima

*/



// CREATE PAGE

add_filter( 'mb_settings_pages', 'prefix_options_page' );

function prefix_options_page($settings_pages){



  $settings_pages[] = array(

    'id'          => 'theme-options',

    'option_name' => 'options_gerais',

    'menu_title'  => 'Página home',

  );

  

  return $settings_pages;



}



// START DEFINITION OF META BOXES

add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );

function prefix_options_meta_boxes( $meta_boxes ) {



  $meta_boxes[] = array(

    'id'        => 'gtm_codes',

    'title'     => 'GTM Code',

    'context'   => 'side',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(



      array(

        'name'  => '',

        'id'    => 'gtm_head',

        'desc'  => 'Code Head',

        'type'  => 'textarea',

      ),



      array(

        'name'  => '',

        'id'    => 'gtm_body',

        'desc'  => 'Code Body',

        'type'  => 'textarea',

      ),

    )

  );
  
  $meta_boxes[] = array(

    'id'        => 'banner_home',

    'title'     => 'SECTION BANNER',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(

      array(
        'name' => '',
        'id'   => 'banner_home_img',
        'type' => 'file_advanced',
        'desc' => 'Imagem destacada do Banner',
        'max_file_uploads' => 1,
      ),

      array(
        'name' => 'Titulo',
        'id'   => 'banner_home_title',
        'type' => 'textarea',
      ),  

      array(
        'name' => 'Descrição',
        'id'   => 'banner_home_sub',
        'type' => 'textarea',
      ),  

      array(
        'id'     => 'banner_home_itens',
        'name'   => '', // Optional
        'type'   => 'group',
        'clone' => true,
        'sort_clone' => true,
        'collapsible' => true,
        'max_clone' => 2,
        'group_title' => 'Botão',
        'save_state' => true,
        // List of sub-fields
        'fields' => array(

      array(
        'name' => 'Texto do Botão',
        'id'   => 'banner_home_cta_name',
        'type' => 'text',
      ),  

      array(
        'name' => 'Link do Botão',
        'id'   => 'banner_home_cta_link',
        'type' => 'text',
      ),  

      array(
        'name'          => 'Cor do botão',
        'id'            => 'banner_home_cta_color',
        'type'          => 'color',
        // Add alpha channel?
        'alpha_channel' => true,
        // Color picker options. See here: https://automattic.github.io/Iris/.
        'js_options'    => array(
            'palettes' => array( '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' )
        ),
      )

      )
      ),

      
      // array(
      //   'name' => '',
      //   'id'   => 'banner_home_icon1',
      //   'type' => 'file_advanced',
      //   'desc' => 'Imagem do primeiro ícone',
      //   'max_file_uploads' => 1,
      // ),

      // array(
      //   'name' => '',
      //   'id'   => 'banner_home_icon2',
      //   'type' => 'file_advanced',
      //   'desc' => 'Imagem do segundo ícone',
      //   'max_file_uploads' => 1,
      // ),

    ),
  );

  $meta_boxes[] = array(

    'id'        => 'cursos_home',

    'title'     => 'SECTION CURSOS',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(
      array(
        'name' => 'Título',
        'id'   => 'cursos_home_title',
        'type' => 'text',
      ),  

      array(
        'name' => 'Descrição',
        'id'   => 'cursos_home_sub',
        'type' => 'textarea',
      ),  

      array(
        'name'          => 'Cor do botão',
        'id'            => 'cursos_home_cta_color',
        'type'          => 'color',
        'alpha_channel' => true,
        'js_options'    => array(
            'palettes' => array( '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' )
      ),
    ),

      array(
        'name' => 'Texto CTA',
        'id'   => 'cursos_home_cta',
        'type' => 'text',
      ),  

    ),
  );
  
  $meta_boxes[] = array(

    'id'        => 'modalidades_home',

    'title'     => 'SECTION MODALIDADES',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(
      array(
        'name' => 'Titulo',
        'id'   => 'modalidades_home_title',
        'type' => 'text',
      ),  

      array(
        'name' => 'Descrição',
        'id'   => 'modalidades_home_sub',
        'type' => 'textarea',
      ),  

    ),
  );

  $meta_boxes[] = array(

    'id'        => 'aprendizado_home',

    'title'     => 'SECTION APRENDIZADO',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(

      array(
        'name' => '',
        'id'   => 'apreendizado_home_img',
        'type' => 'file_advanced',
        'desc' => 'Imagem destacada',
        'max_file_uploads' => 1,
      ),

      array(
        'name' => '',
        'id'   => 'apreendizado_home_gif',
        'type' => 'file_advanced',
        'desc' => 'Gif animado',
        'max_file_uploads' => 1,
      ),

      array(
        'name' => 'Título',
        'id'   => 'aprendizado_home_title',
        'type' => 'textarea',
      ),  

      array(
        'name' => 'Descrição',
        'id'   => 'aprendizado_home_sub',
        'type' => 'textarea',
      ),

      array(
        'name' => 'Texto do botão',
        'id'   => 'aprendizado_home_cta',
        'type' => 'text',
      ), 

      array(
        'name'          => 'Cor do botão',
        'id'            => 'aprendizado_home_color',
        'type'          => 'color',
        // Add alpha channel?
        'alpha_channel' => true,
        // Color picker options. See here: https://automattic.github.io/Iris/.
        'js_options'    => array(
            'palettes' => array( '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' )
          ),
      ),

      

    ),
  );

  $meta_boxes[] = array(

    'id'        => 'alunos_home',

    'title'     => 'SECTION ALUNOS',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(
      array(
        'name' => 'Titulo',
        'id'   => 'alunos_home_title',
        'type' => 'textarea',
      ),  

      array(
        'id'     => 'alunos_home_blocos',
        'name'   => '', // Optional
        'type'   => 'group',
        'clone' => true,
        'sort_clone' => true,
        'collapsible' => true,
        'max_clone' => 6,
        'group_title' => 'Depoimentos',
        'save_state' => true,
        // List of sub-fields
        'fields' => array(
          array(
            'name' => '',
            'id'   => 'alunos_home_blocos_icon',
            'type' => 'file_advanced',
            'desc' => 'Imagem destacada do Estudante',
            'max_file_uploads' => 1,
          ),
          array(
            'id'   => 'alunos_home_blocos_desc',
            'name' => 'Comentário do aluno',
            'type' => 'textarea',
          ),

          array(
            'id'   => 'alunos_home_blocos_name',
            'name' => 'Nome do aluno',
            'type' => 'text',
          ),

          array(
            'id'   => 'alunos_home_blocos_region',
            'name' => 'Região',
            'type' => 'text',
          ),
            
        ),
      ),


    ),
  );

  $meta_boxes[] = array(

    'id'        => 'blog_home',

    'title'     => 'SECTION BLOG',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(
      array(
        'name' => 'Titulo',
        'id'   => 'blog_home_title',
        'type' => 'text',
      ),  

      array(
        'name' => 'Descrição',
        'id'   => 'blog_home_sub',
        'type' => 'textarea',
      ),  

      array(
        'name' => 'Texto do botão',
        'id'   => 'blog_home_cta',
        'type' => 'text',
      ),  

      array(
        'name'          => 'Cor do botão',
        'id'            => 'blog_home_cta_color',
        'type'          => 'color',
        'alpha_channel' => true,
        'js_options'    => array(
            'palettes' => array( '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' )
      ),
    ),

      array(
        'name' => 'Link do botão',
        'id'   => 'blog_home_link',
        'type' => 'text',
      ),  

    ),
  );

  $meta_boxes[] = array(

    'id'        => 'aulas_home',

    'title'     => 'SECTION NÃO PERCA MAIS TEMPO',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(

      array(
        'name' => '',
        'id'   => 'aulas_home_img',
        'type' => 'file_advanced',
        'desc' => '',
        'max_file_uploads' => 1,
      ),

      array(
        'name' => 'Titulo',
        'id'   => 'aulas_home_title',
        'type' => 'textarea',
      ),  

      array(
        'name' => 'Descrição',
        'id'   => 'aulas_home_sub',
        'type' => 'textarea',
      ),  

      array(
        'name' => 'Texto do botão',
        'id'   => 'aulas_home_cta',
        'type' => 'text',
      ),  

      array(
        'name'          => 'Cor do botão',
        'id'            => 'aulas_home_cta_color',
        'type'          => 'color',
        'alpha_channel' => true,
        'js_options'    => array(
            'palettes' => array( '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' )
      ),
    ),

      array(
        'name' => 'link do botão',
        'id'   => 'aulas_home_link',
        'type' => 'text',
      ),  

    ),
  );


  $meta_boxes[] = array(

    'id'        => 'footer_home',

    'title'     => 'SECTION FOOTER',

    'context'   => 'normal',

    'priority'  => 'high',

    'settings_pages' => 'theme-options',

    'fields'    => array(
      array(
        'id'     => 'footer_home_sociais_bloco',
        'name'   => '', // Optional
        'type'   => 'group',
        'clone' => true,
        'sort_clone' => true,
        'collapsible' => true,
        'max_clone' => 4,
        'group_title' => 'Redes Sociais',
        'save_state' => true,
        // List of sub-fields
        'fields' => array(
          array(
            'name' => '',
            'id'   => 'footer_home_social_img',
            'type' => 'file_advanced',
            'desc' => 'Imagem da rede social',
            'max_file_uploads' => 1,
          ),
          array(
            'id'   => 'footer_home_social_link',
            'name' => 'Link da rede Social',
            'type' => 'text',
          ),
            
        ),
      ),

    ),
  );
  
  return $meta_boxes;

}