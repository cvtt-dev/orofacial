<?php



    $prefix = 'wpcf_';

    

    add_filter('rwmb_meta_boxes', 'wpcf_meta_boxes');

    

    function wpcf_meta_boxes($meta_boxes) {

      $meta_boxes[] = array(
        'id'             => 'cursos',
        'title'          => 'INFORMAÇÕES DO CURSO',
        'context'        => 'normal',
        'post_types' => array('cursos'),
        'fields'     => array(
        array(
            'name' => 'Quantidade de Horas',
            'id'   => 'component_cursos_hour',
            'type' => 'text',
            ),

        array(
          'name' => 'Quantidade de dias',
          'id'   => 'component_cursos_day',
          'type' => 'text',
          ),

        array(
          'name' => 'Preço anterior',
          'id'   => 'component_cursos_price',
          'type' => 'text',
            ),

        array(
        'name' => 'Preço atual',
        'id'   => 'component_cursos_price_new',
        'type' => 'text',
        ),

        array(
          'id'   => 'single_cursos_painel_desc',
          'name' => 'Descrição',
          'type' => 'textarea',
        ),

        array(
          'id'   => 'single_cursos_painel_cta',
          'name' => 'CTA texto',
          'type' => 'text',
        ),

        array(
          'id'   => 'single_cursos_painel_link',
          'name' => 'CTA link',
          'type' => 'text',
        ),
          
        array(
          'id'     => 'single_cursos_painel_itens',
          'name'   => '', // Optional
          'type'   => 'group',
          'clone' => true,
          'sort_clone' => true,
          'collapsible' => true,
          'max_clone' => 8,
          'group_title' => 'Item da lista',
          'save_state' => true,
          // List of sub-fields
          'fields' => array(

            array(
              'name' => '',
              'id'   => 'single_cursos_painel_icon',
              'type' => 'file_advanced',
              'desc' => 'Icone da lista',
              'max_file_uploads' => 1,
            ),
          
            array(
              'id'   => 'single_cursos_painel_txt',
              'name' => '',
              'type' => 'text',
            ),
  
          ),
        ),

        )
      );

      $meta_boxes[] = array(
        'id'             => 'single_cursos_video',
        'title'          => 'VIDEO DO CURSO',
        'context'        => 'normal',
        'post_types' => array('cursos'),
        'fields'     => array(

          array(
            'name' => '',
            'id'   => 'single_cursos_video_thumb',
            'type' => 'file_advanced',
            'desc' => 'Thumb do Vídeo',
            'max_file_uploads' => 1,
          ),
            
          array(
            'id'   => 'single_curso_video_link',
            'name' => 'Link do video',
            'type' => 'text',
            ),
            
          ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_cursos_sobre',
        'title'          => 'SOBRE O CURSO',
        'context'        => 'normal',
        'post_types' => array('cursos'),
        'fields'     => array(

          array(
            'id'   => 'single_cursos_sobre_title',
            'name' => 'Título',
            'type' => 'text',
          ),

          array(
            'id'   => 'single_cursos_sobre_desc',
            'name' => 'Descrição',
            'type' => 'wysiwyg',
            ),

          ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_cursos_prof',
        'title'          => 'COM QUEM VOCÊ VAI APRENDER',
        'context'        => 'normal',
        'post_types' => array('cursos'),
        'fields'     => array(
            
          array(
            'id'   => 'single_cursos_prof_title',
            'name' => 'Título',
            'type' => 'text',
            ),
            
            array(
              'id'     => 'single_curos_prof_item',
              'name'   => '', // Optional
              'type'   => 'group',
              'clone' => true,
              'sort_clone' => true,
              'collapsible' => true,
              'max_clone' => 3,
              'group_title' => 'PROFESSOR',
              'save_state' => true,
              // List of sub-fields
              'fields' => array(

                array(
                  'name' => '',
                  'id'   => 'single_curos_prof_img',
                  'type' => 'file_advanced',
                  'desc' => 'Foto do professor',
                  'max_file_uploads' => 1,
                ),
              
                array(
                  'id'   => 'single_cursos_prof_nome',
                  'name' => 'Nome do professor',
                  'type' => 'text',
                ),

                array(
                  'id'   => 'single_cursos_prof_uni',
                  'name' => 'Universidade',
                  'type' => 'text',
                ),

                array(
                  'id'   => 'single_cursos_prof_desc',
                  'name' => 'Descrição',
                  'type' => 'textarea',
                ),
      
               
                  
              ),
            ),
              
          ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_cursos_modulos',
        'title'          => 'MODULOS',
        'context'        => 'normal',
        'post_types' => array('cursos'),
        'fields'     => array(

          array(
            'id'     => 'single_cursos_modulos_itens',
            'name'   => '', // Optional
            'type'   => 'group',
            'clone' => true,
            'sort_clone' => true,
            'collapsible' => true,
            'max_clone' => 3,
            'group_title' => 'Módulo',
            'save_state' => true,
            // List of sub-fields
            'fields' => array(

          array(
            'id'   => 'single_cursos_modulo_model',
            'name' => 'Título',
            'type' => 'text',
          ),

          array(
            'id'     => 'single_cursos_modulos_item',
            'name'   => '', // Optional
            'type'   => 'group',
            'clone' => true,
            'sort_clone' => true,
            'collapsible' => true,
            'max_clone' => 2,
            'group_title' => 'Parte',
            'save_state' => true,
            // List of sub-fields
            'fields' => array(

              array(
                'id'   => 'single_cursos_modulo_state',
                'name' => 'Status',
                'type' => 'text',
              ),

              array(
                'id'   => 'single_cursos_modulo_tema',
                'name' => 'Título',
                'type' => 'textarea',
              ),

              array(
                'id'   => 'single_cursos_modulo_desc',
                'name' => 'Descrição',
                'type' => 'wysiwyg',
              ),
             
                
            ),
          ),


          ),
          ),
          
            
          ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_cursos_participe',
        'title'          => 'PARTICIPE DA REVOLUÇÃO',
        'context'        => 'normal',
        'post_types' => array('cursos'),
        'fields'     => array(

          array(
            'id'   => 'single_eventos_participe_title',
            'name' => 'Título',
            'type' => 'textarea',
          ),
            
          array(
            'id'   => 'single_eventos_participe_desc',
            'name' => 'Descrição',
            'type' => 'textarea',

          ),

          array(
            'id'   => 'single_eventos_participe_cta',
            'name' => 'Texto do botão',
            'type' => 'text',

          ),

          array(
            'id'   => 'single_eventos_participe_link',
            'name' => 'Link',
            'type' => 'text',
          ),

          array(
            'name'          => 'Cor do botão',
            'id'            => 'single_eventos_participe_color',
            'type'          => 'color',
            'alpha_channel' => true,

          ),

        ),
      );

      $meta_boxes[] = array(

        'id'         => 'cursos_destaque',

        'title'      => 'Destaque',

        'post_types' => array('cursos'),

        'context'    => 'side',

        'priority'   => 'high',

        'fields'     => array ( 


          array(

            'id'        => "cursos_destaque_option",

            'name'      => '',

            'type'      => 'radio',

            'options'   => array('1' => 'Sim', '0' => 'Nao'),

            'std'       => '0',

            'admin_columns' => 'after title',

          ),
        ),
      );

      $meta_boxes[] = array(

          'id'         => 'alunos_single_cursos',
  
          'title'      => 'SECTION ALUNOS',
  
          'post_types' => array('cursos'),
  
          'context'    => 'normal',
  
          'priority'   => 'high',
  
          'fields'     => array ( 

          array(
            'name' => 'Titulo',
            'id'   => 'single_alunos_cursos_title',
            'type' => 'textarea',
          ),  
    
          array(
            'id'     => 'single_alunos_cursos_blocos',
            'name'   => '', // Optional
            'type'   => 'group',
            'clone' => true,
            'sort_clone' => true,
            'collapsible' => true,
            'max_clone' => 6,
            'group_title' => 'Depoimentos',
            'save_state' => true,
            // List of sub-fields
            'fields' => array(
              array(
                'name' => '',
                'id'   => 'alunos_single_cursos_blocos_icon',
                'type' => 'file_advanced',
                'desc' => 'Imagem destacada do Estudante',
                'max_file_uploads' => 1,
              ),
              array(
                'id'   => 'alunos_single_cursos_blocos_desc',
                'name' => 'Comentário do aluno',
                'type' => 'textarea',
              ),
    
              array(
                'id'   => 'alunos_single_cursos_blocos_name',
                'name' => 'Nome do aluno',
                'type' => 'text',
              ),
    
              array(
                'id'   => 'alunos_single_cursos_blocos_region',
                'name' => 'Região',
                'type' => 'text',
              ),
                
            ),
          ),
    
    
        ),
      );



      // Eventos

      $meta_boxes[] = array(
        'id'             => 'component_evento',
        'title'          => 'INFORMAÇÔES DO EVENTO',
        'context'        => 'normal',
        'post_types' => array('eventos'),
        'fields'     => array(

          array(
            'id'   => 'component_evento_desc',
            'name' => 'Descricão',
            'type' => 'textarea',
          ),

          array(
            'id'   => 'component_evento_nday',
            'name' => 'Dia',
            'type' => 'text',
          ),

          array(
            'id'   => 'component_evento_month',
            'name' => 'Mês (por extenso)',
            'type' => 'text',
          ),

          array(
            'id'   => 'component_evento_hour',
            'name' => 'Horário',
            'type' => 'text',
          ),

          array(
            'id'   => 'component_evento_city',
            'name' => 'Cidade',
            'type' => 'text',
          ),

          array(
            'id'   => 'component_evento_cta',
            'name' => 'Texto CTA',
            'type' => 'text',
          ),

          array(
            'id'   => 'component_evento_cta_link',
            'name' => 'Link CTA',
            'type' => 'text',
          ),

          array(
            'id'     => 'single_event_painel_itens',
            'name'   => '', // Optional
            'type'   => 'group',
            'clone' => true,
            'sort_clone' => true,
            'collapsible' => true,
            'max_clone' => 8,
            'group_title' => 'Item da lista',
            'save_state' => true,
            // List of sub-fields
            'fields' => array(
  
              array(
                'name' => '',
                'id'   => 'single_event_painel_icon',
                'type' => 'file_advanced',
                'desc' => 'Icone da lista',
                'max_file_uploads' => 1,
              ),
            
              array(
                'id'   => 'single_event_painel_txt',
                'name' => '',
                'type' => 'text',
              ),
    
            ),
          ),
            
        ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_eventos_video',
        'title'          => 'VIDEO DO EVENTO',
        'context'        => 'normal',
        'post_types' => array('eventos'),
        'fields'     => array(

          array(
            'name' => '',
            'id'   => 'single_eventos_video_thumb',
            'type' => 'file_advanced',
            'desc' => 'Thumb do Vídeo',
            'max_file_uploads' => 1,
          ),
            
          array(
            'id'   => 'single_eventos_video_link',
            'name' => 'id do video',
            'type' => 'text',
            ),
            
          ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_eventos_agenda',
        'title'          => 'AGENDA DO EVENTO',
        'context'        => 'normal',
        'post_types' => array('eventos'),
        'fields'     => array(

          array(
            'id'   => 'single_eventos_agenda_title',
            'name' => 'Texto',
            'type' => 'text',
          ),
            
            
          array(
            'id'   => 'single_eventos_agenda_desc',
            'name' => 'Descrição',
            'type' => 'wysiwyg',
          ),
            
          ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_eventos_participantes',
        'title'          => 'PARTICIPANTES',
        'context'        => 'normal',
        'post_types' => array('eventos'),
        'fields'     => array(

          array(
            'id'   => 'single_eventos_participantes_title',
            'name' => 'Título',
            'type' => 'text',
          ),

          array(
            'id'     => 'single_eventos_participantes_itens',
            'name'   => '', // Optional
            'type'   => 'group',
            'clone' => true,
            'sort_clone' => true,
            'collapsible' => true,
            'max_clone' => 3,
            'group_title' => 'Professor',
            'save_state' => true,
            // List of sub-fields
            'fields' => array(

              array(
                'name' => '',
                'id'   => 'single_eventos_participantes_thumb',
                'type' => 'file_advanced',
                'desc' => 'Foto do professor',
                'max_file_uploads' => 1,
              ),
            
              array(
                'id'   => 'single_eventos_participantes_name',
                'name' => 'Nome do professor',
                'type' => 'text',
              ),

              array(
                'id'   => 'single_eventos_participantes_uni',
                'name' => 'Universidade',
                'type' => 'text',
              ),

              array(
                'id'   => 'single_eventos_participantes_desc',
                'name' => 'descrição',
                'type' => 'textarea',
              ),
    
             
                
            ),
          ),
            
          
            
          ),
      );

      $meta_boxes[] = array(
        'id'             => 'single_eventos_revolucao',
        'title'          => 'SECTION PARTICIPE DA REVOLUÇÃO',
        'context'        => 'normal',
        'post_types' => array('eventos'),
        'fields'     => array(

          array(
            'id'   => 'single_eventos_revolucao_title',
            'name' => 'Título',
            'type' => 'textarea',
          ),

          array(
            'id'   => 'single_eventos_revolucao_desc',
            'name' => 'Descrição',
            'type' => 'textarea',
          ),
            
          array(
            'id'   => 'single_eventos_revolucao_cta',
            'name' => 'Texto do Botão',
            'type' => 'text',
          ),

          array(
            'id'   => 'single_eventos_revolucao_link',
            'name' => 'Link do botão',
            'type' => 'text',
          ),

          array(
            'name'          => 'Cor do botão',
            'id'            => 'single_eventos_revolucao_color',
            'type'          => 'color',
            'alpha_channel' => true,
          ),
            
        ),
      );

      $meta_boxes[] = array(

        'id'         => 'alunos_single_eventos',

        'title'      => 'SECTION ALUNOS',

        'post_types' => array('eventos'),

        'context'    => 'normal',

        'priority'   => 'high',

        'fields'     => array ( 

        array(
          'name' => 'Titulo',
          'id'   => 'alunos_single_eventos_title',
          'type' => 'textarea',
        ),  
  
        array(
          'id'     => 'alunos_single_eventos_blocos',
          'name'   => '', // Optional
          'type'   => 'group',
          'clone' => true,
          'sort_clone' => true,
          'collapsible' => true,
          'max_clone' => 6,
          'group_title' => 'Depoimentos',
          'save_state' => true,
          // List of sub-fields
          'fields' => array(
            array(
              'name' => '',
              'id'   => 'alunos_single_eventos_blocos_icon',
              'type' => 'file_advanced',
              'desc' => 'Imagem destacada do Estudante',
              'max_file_uploads' => 1,
            ),
            array(
              'id'   => 'alunos_single_eventos_blocos_desc',
              'name' => 'Comentário do aluno',
              'type' => 'textarea',
            ),
  
            array(
              'id'   => 'alunos_single_eventos_blocos_name',
              'name' => 'Nome do aluno',
              'type' => 'text',
            ),
  
            array(
              'id'   => 'alunos_single_eventos_blocos_region',
              'name' => 'Região',
              'type' => 'text',
            ),
              
          ),
        ),
  
  
      ),
    );
      
      return $meta_boxes;

}

