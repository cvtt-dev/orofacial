<?php



    add_filter('rwmb_meta_boxes', 'wpcf_meta_boxes_tax');

    function wpcf_meta_boxes_tax($meta_boxes) {

        $meta_boxes[] = array(

            'id' => 'term_modalidade',

            'title'      => 'Campos',

            'taxonomies' => 'modalidade', // List of taxonomies. Array or string

            'fields' => array(



                array(

                    'id'   => 'term_modalidade_thumb',

                    'name' => 'Imagem da Categoria',

                    'type' => 'image_advanced',

                ),

                array(
                    'name'          => 'Cor',
                    'id'            => 'term_modalidade_thumb_color',
                    'type'          => 'color',
                    'alpha_channel' => true,
                    
                  ),
                

            ),

        );


        return $meta_boxes;

    }