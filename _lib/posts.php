<?php

/**

* Custom Post Types

* Desenvolvedor: Bruno Lima

*/

function post_type_cursos_register() {

    $labels = array(
        'name'          => 'Cursos',
        'singular_name' => 'Curso',
        'menu_name'     => 'Cursos',
        'add_new'       => _x('Adicionar Cursos', 'item'),
        'add_new_item'  => __('Adicionar Novo Curso'),
        'edit_item'     => __('Editar curso'),
        'new_item'      => __('Novo curso')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'cursos'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'supports' => array('title','editor', 'thumbnail', 'excerpt')
    );
    register_post_type('cursos', $args);

}
add_action('init', 'post_type_cursos_register');


function post_type_eventos_register() {

    $labels = array(
        'name'          => 'Eventos',
        'singular_name' => 'Evento',
        'menu_name'     => 'Eventos',
        'add_new'       => _x('Adicionar Eventos', 'item'),
        'add_new_item'  => __('Adicionar Novo Evento'),
        'edit_item'     => __('Editar evento'),
        'new_item'      => __('Novo evento')
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'eventos'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 2,
        'menu_icon' => 'dashicons-calendar-alt',
        'supports' => array('title','editor', 'thumbnail', 'excerpt')
    );
    register_post_type('eventos', $args);

}
add_action('init', 'post_type_eventos_register');
