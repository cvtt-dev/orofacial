(function() {

    tinymce.PluginManager.add('custom_mce_button', function(editor, url) {
        editor.addButton('custom_mce_button', {
            icon: 'image',
            text: 'Banners',
            onclick: function() {
                
                jQuery(function($) {
                    $(document).ready(function() {
                       
                
                        $.ajax({
                            url: window.ajaxurl,
                            type: 'GET',
                            dataType: 'html',
                            data: "action=estados_banner",
                
                            beforeSend: function () {
                                console.log('buscando...');
                            },
                            
                            success: function (dados) {
                                
                                var dados   = JSON.parse(dados);                    
                                //console.log(dados);
                                var estados = dados.estados[0];
                                
                                console.log(estados);

                                editor.windowManager.open({
                                    title: 'Inserir Banner',
                                    body: [
                                        {
                                            type: 'listbox',
                                            name: 'zona',
                                            label: 'Zona',
                                            values: [
                                                
                                                {
                                                    text: 'Interna',
                                                    value: 'interna'
                                                },  
                                            ]
                                        }, 
                                        {
                                            type: 'listbox',
                                            name: 'slug',
                                            label: 'Página slug',
                                            values: [
                                                {
                                                    text: 'Matérias',
                                                    value: 'materias'
                                                }, 
                                                {
                                                    text: 'Notícias',
                                                    value: 'noticias'
                                                }, 
                                            ]
                                        }, 
                                        // {
                                        //     type: 'listbox',
                                        //     name: 'ceara',
                                        //     label: 'CE',
                                        //     values: [
                                        //         {
                                        //             text: 'Nenhum',
                                        //             value: ''
                                        //         }, 
                                        //         {
                                        //             text: 'Ceara',
                                        //             value: 'ceara'
                                        //         }, 
                                        //     ]
                                        // },  
                                        // {
                                        //     type: 'listbox',
                                        //     name: 'minas-gerais',
                                        //     label: 'MG',
                                        //     values: [
                                        //         {
                                        //             text: 'Nenhum',
                                        //             value: ''
                                        //         }, 
                                        //         {
                                        //             text: 'Minas Gerais',
                                        //             value: 'minas-gerais'
                                        //         }, 
                                        //     ]
                                        // },
                                        // {
                                        //     type: 'listbox',
                                        //     name: 'paraiba',
                                        //     label: 'PB',
                                        //     values: [
                                        //         {
                                        //             text: 'Nenhum',
                                        //             value: ''
                                        //         }, 
                                        //         {
                                        //             text: 'Paraíba',
                                        //             value: 'paraipa'
                                        //         }, 
                                        //     ]
                                        // },                                            
                                        {
                                            type: 'listbox',
                                            name: 'estados',
                                            label: 'Estados',
                                            values: [
                                                {
                                                    text: 'Todos',
                                                    value: estados[0].slug+','+estados[1].slug+','+estados[2].slug+','+estados[3].slug+','+estados[4].slug+','+estados[5].slug+','+estados[6].slug+','+estados[7].slug
                                                },
                                                {
                                                    text: estados[1].name,
                                                    value: estados[1].slug
                                                }, 
                                                {
                                                    text: estados[2].name,
                                                    value: estados[2].slug
                                                },
                                                {
                                                    text: estados[3].name,
                                                    value: estados[3].slug
                                                },  
                                            ]
                                        }, 
                                        
                                    ],
                                    onsubmit: function(e) {
                                        editor.insertContent(
                                            '[banner_zone zona="'+e.data.zona +'" slug="'+ e.data.slug +'" estados="' + e.data.estados +'"]' + editor.selection .getContent() +'[/banner_zone]'
                                            //'[banner_zone zona="'+e.data.zona +'" slug="'+ e.data.slug +'" estados="' + e.data.ceara +','+ e.data['minas-gerais'] +','+ e.data.paraiba+'"]' + editor.selection .getContent() +'[/banner_zone]'
                                        );
                                    }
                                });
                                
                            }
                        });
                
                
                    });
                });
                
                

                
            }
        });
    });
})();