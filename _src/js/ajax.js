jQuery(function($) {
    $(document).ready(function() {
        
        var SITE_URL = $('.header-img').attr('href');
        var AJAX_URL = SITE_URL + '/wp-admin/admin-ajax.php';  

        function getPostagem(id,name) {

            $.ajax({
                url: AJAX_URL,
                type: 'POST',
                dataType: 'html',
                data: "action=getPost&categoria="+id+"&name="+name,

                beforeSend: function () {
                    $('.msg-popup').fadeIn(250);
                    $('.load').text('Carregando '+name);
                },

                success: function (response) {
                    
                    var postagem      = JSON.parse(response);
                    var html_postagem = postagem.html_postagem[0];

                    $('.msg-popup').delay(250).fadeOut(500);

                    if(html_postagem) {
                        $('#show-cursos').html(html_postagem);
                    } else {
                        $('#show_cursos').html(
                            '<div class="escolas-ajax-msg"><h2 class="escolas-ajax-msg__title">Nenhum curso Encontrada.</h2><p class="escolas-ajax-msg__desc">Selecione o estado, cidade e o tipo da sua escola!</p></div>');
                    }
                    

                }
            });

            return false;
        }


        function getCategoria() {
            
            $('.mfcc-component-blog-nav__link').eq(0).addClass('is_active');
            
            $('.mfcc-component-blog-nav').find('.mfcc-component-blog-nav__link').each(function(index) {
                
                
                $(this).on('click',function(e) {
                    e.preventDefault();
                    
                    $('.mfcc-component-blog-nav').find('.mfcc-component-blog-nav__link').eq(0).removeClass('is_active');
                    
                    //var href  = $(this).attr('href');
                    var id    = $(this).attr('id');
                    var name  = $(this).attr('data-name');
                    //var tab   = $(this).attr('data-tab');
                    
                    getPostagem(id,name);
                })
                    
            });
           
        } getCategoria();

        


        
    });
});
