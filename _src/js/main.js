jQuery(function($) {
  $(document).ready(function() {

    function menuHeaderMobile() {
            
      var elBtn    = $("#menu-header-mobile-button");
      
      elBtn.on("click", function() {

        
        var elMenu   = $(".component-menu-mobile");
        
        if ($(this).hasClass('is-active')) {
          
          $(this).removeClass('is-active');
          
          elMenu.hide();
          
        } else {
          
          $(this).addClass('is-active');

          elMenu.show();

        }
          
      });


    } menuHeaderMobile();


    $('.owl-carousel-modalidades').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      autoplay:true,
      autoplayTimeout:2000,
      autoplayHoverPause:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:4
          }
      }
    })

    $(".fancybox").fancybox();

    function heightMaterial() {
      
      var maxHeight = 0;
      var elMaterial = $('.section-blog-card__sub');
      
      $(elMaterial).each(function(){
        if ($(this).height() > maxHeight) { 
          maxHeight = $(this).height(); 
        }
      });

      $(elMaterial).height(maxHeight);
      
    } heightMaterial();



    // var windw = this;

    // $.fn.followTo = function ( pos ) {
    //     var $this = this,
    //         $window = $(windw);
        
    //     $window.scroll(function(e){
    //         if ($window.scrollTop() > pos) {
    //             $this.css({
    //                 position: 'absolute',
    //                 top: pos
    //             });
    //         } else {
    //             $this.css({
    //                 position: 'fixed',
    //                 top: 130,
    //             });
    //         }
    //     });
    // };

    // $('.card-course').followTo(2500);




  });
});
