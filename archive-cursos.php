<?php get_template_part('templates/html','header');?>
<?php $settings = get_option('options_gerais'); ?>

<div class="section hero section-wrapper">
    <div class="main-container">
      <div class="header-interna">
        <div class="hero__content hero__interna">
          <h1 class="title-heading">Cursos</h1>
          <p class="text-large center">Uma escola de cursos livres, criada para torna-lo mais criativo, subversivo, sensível e do bem.</p>
        </div>
      </div>
      <div class="bg shape-2"></div>
      <div class="bg shape-1"></div>
    </div>
  </div>
  <div class="section section-cursos">
    <div class="main-container main-container-small">
      <div id="filters" class="filters">
        <div class="button-group w-clearfix">
          <div class="filters-wrap mfcc-component-blog-nav">
            <!-- <a href="#" class="button button-white button-check w-button">Todos</a> -->

            <?php

            $terms = get_terms(array(
              'taxonomy' =>'modalidade',
              'hide_empty' => false
            ));

            foreach($terms as $term):
              $thumb = get_term_meta( $term->term_id, 'term_modalidade_thumb', true);
              $color = get_term_meta( $term->term_id, 'term_modalidade_thumb_color', true);
              $img = wp_get_attachment_image_src($thumb, 'medium_large'); 
            ?>

            <a href="<?php echo get_term_link($term->term_id); ?>" data-name="<?php echo $term->name; ?>" id="<?php echo $term->term_id; ?>" class="button button-white w-button mfcc-component-blog-nav__link"><?php echo $term->name; ?></a>

            <?php endforeach; ?>
        
          </div>
          
          <div class="search-wrapper w-form">
            <form role="search" method="get" class="escola-filtro search-form" action="<?php echo home_url( '/' ); ?>">
              <div class="escola-filtro__field escola-filtro__field--search">
                  <input type="search" class="quicksearch w-input" placeholder="<?php echo esc_attr_x( 'Buscar …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                  <input type="hidden" name="post_type" value="cursos" />
                  <!-- <input type="submit" class="escola-filtro__icon escola-filtro__icon--search search-submit" value="" /> -->
              </div>
          </form>
        </div>

          <div class="msg-popup"><div class="wrap-load"><div class="load">Carregando...</div></div></div>

      </div>
          <div class="w-layout-grid image-link-box-grid image-link-split" id="show-cursos">

          <?php     
              $args = array('post_type' => 'cursos', 'posts_per_page' => 6, 'orderby' => 'date', 'order' => 'DESC' );
              $loop = new WP_Query($args); 
              while ($loop->have_posts()) : $loop->the_post(); 
              $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
              $hour = get_post_meta(get_the_ID(), 'component_cursos_hour', true);
              $day = get_post_meta(get_the_ID(), 'component_cursos_day', true);
              $price = get_post_meta(get_the_ID(), 'component_cursos_price', true);
              $new_price = get_post_meta(get_the_ID(), 'component_cursos_price_new', true);
          ?>

            <div id="post-<?php the_ID(); ?>" class="mfcc-component-blog-postagens container-small align-center">
              <a href="<?php the_permalink();?>" class="image-link-box w-inline-block"><img src="<?php if($thumbnail_url): echo $thumbnail_url; else: echo ""; endif; ?>" sizes="(max-width: 479px) 91vw, 400px" alt="" class="image-course">
                <div class="boxed square-top boxed-small">
                  <div class="image-link-box-content">
                    <div class="title-text"><?php echo mb_strimwidth(the_title(),0,50,'...'); ?></div>
                    <div class="description">
                      <p class="paragraph"><?php echo mb_strimwidth(the_content(),0,50,'...'); ?></p>
                    </div>
                    <div class="divider"></div>
                    <div class="details">
                    <?php foreach($settings['cursos_home_img'] as $img): ?>
                      <?php $url = wp_get_attachment_image_url($img,'full','',array('class'=>''));?>
                    <?php endforeach; ?>
                      <div class="category card-course-curso__div"><img src="<?php echo  $url; ?>" loading="lazy" alt="" class="icon saturation">
                        <div class="category-text"><?= $hour ?>h / <?= $day ?> dias</div>
                      </div>
                      <div class="price">
                        <div class="price-text"><span class="mkt-price">R$<?= $price ?></span> R$<?= $new_price ?></div>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>

            <?php endwhile; wp_reset_postdata();?>

          </div>
        </div>
      </div>
    </div>
    <div class="divider w98"></div>
  </div>

  <?php get_template_part('templates/frontpage','alunos'); ?>
  <?php get_template_part('templates/frontpage','destaque'); ?>
  <?php get_template_part('templates/html','footer'); ?>