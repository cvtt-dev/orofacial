<?php $settings = get_option('options_gerais'); ?>

<?php get_template_part('templates/html','header');?>

<?php     
    $categorias = get_terms( array( 'taxonomy' => 'modalidade', 'hide_empty' => false ));
    $cat      = $categorias[0]->name;

    while (have_posts()) : the_post(); $loop[] = get_the_ID();
    $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
    $hour = get_post_meta(get_the_ID(), 'component_cursos_hour', true);
    $day = get_post_meta(get_the_ID(), 'component_cursos_day', true);
    $price = get_post_meta(get_the_ID(), 'component_cursos_price', true);
    $new_price = get_post_meta(get_the_ID(), 'component_cursos_price_new', true);
  
    $sobre_desc = get_post_meta(get_the_ID(), 'single_cursos_sobre_desc', true);

    $list_title = get_post_meta(get_the_ID(), 'single_cursos_list_title', true);
    $list_desc = get_post_meta(get_the_ID(), 'single_cursos_itens', true);

    $method_title = get_post_meta(get_the_ID(), 'single_cursos_method_title', true);
    $method_desc = get_post_meta(get_the_ID(), 'single_cursos_method_desc', true);

    $prof_title = get_post_meta(get_the_ID(), 'single_cursos_prof_title', true);
    $prof_item = get_post_meta(get_the_ID(), 'single_curos_prof_item', true);

    //Video

    $video_img = get_post_meta(get_the_ID(), 'single_eventos_video_thumb', true);
    $thumb = wp_get_attachment_image_src($video_img, 'medium_large');
    $video_link = get_post_meta(get_the_ID(), 'single_curso_video_link', true);

    // painel lateral

    $painel_valor = get_post_meta(get_the_ID(), 'component_cursos_price', true);
    $painel_valor_new = get_post_meta(get_the_ID(), 'component_cursos_price_new', true);
    $painel_desc = get_post_meta(get_the_ID(), 'single_cursos_painel_desc', true);
    $painel_cta = get_post_meta(get_the_ID(), 'single_cursos_painel_cta', true);
    $painel_link = get_post_meta(get_the_ID(), 'single_cursos_painel_link', true);
    $painel_list = get_post_meta(get_the_ID(), 'single_cursos_painel_itens', true);

    // Módulos
    $Modulo = get_post_meta(get_the_ID(), 'single_cursos_modulos_itens', true);

    // Participe da Revolução
    $title_rev = get_post_meta(get_the_ID(), 'single_eventos_participe_title', true);
    $cta_desc = get_post_meta(get_the_ID(), 'single_eventos_participe_desc', true);
    $cta_title = get_post_meta(get_the_ID(), 'single_eventos_participe_cta', true);
    $cta_link = get_post_meta(get_the_ID(), 'single_eventos_participe_link', true);
    $cta_color = get_post_meta(get_the_ID(), 'single_eventos_participe_color', true);

    // Depoimentos
    $aluno_title = get_post_meta(get_the_ID(), 'single_alunos_cursos_title', true);
    $aluno_bloco = get_post_meta(get_the_ID(), 'single_alunos_cursos_blocos', true);
    


    ?>

<div class="section hero">
  <div class="main-container">
    <div class="course-wrapper">
      <div class="features">
        <div class="header-course">
          <div class="text-pill-wrapper">
            <div class="text-pill pill-white">
              <h6 class="heading-pill"><?php echo $cat; ?></h6>
          </div>
      </div>
      <h4 class="display-heading-white"><?php echo the_title(); ?></h4>
      <div class="text-medium text-light"><?php echo get_the_content(); ?></div>
  </div>

  

  <div class="video-wrapper shadow-large space-top space-bottom">
    <img src="<?php if($thumb[0]): echo $thumb[0]; else: echo get_template_directory_uri().'/assets/images/img-padrao.jpg'; endif;?>" alt="" class="">
    <a class="various fancybox video-button w-inline-block" href="<?php if($video_link): echo "https://www.youtube.com/watch?v=".$video_link; else: echo get_template_directory_uri().'/assets/images/img-padrao.jpg'; endif; ?>">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-play.svg" alt="" class="image-3">
    </a>
  </div>

  <div class="card-course-curso">
  <div class="boxed boxed-course">
    <div class="image-link-box-content">
      <div class="price-big"><span class="mkt-price-big">R$ <?= $painel_valor; ?></span> R$ <?= $painel_valor_new; ?></div>
      <div class="description">
        <p class="paragraph"><?= $painel_desc; ?></p>
      </div>
      <a href="<?= $painel_link; ?>" target="_blank" class="hero__btn max_100 buy__btn w-button"><?= $painel_cta; ?></a>
      <div class="divider divider-card-course"></div>
      <div class="details details-interna">

        <?php if($painel_list): foreach($painel_list as $item): ?>

          <?php foreach($item['single_cursos_painel_icon'] as $img): ?>
            <?php $url = wp_get_attachment_image_url($img,'full','',array('class'=>'')); ?>
          <?php endforeach; ?>
        

        <div class="category category-100 card-course-curso__div">

          <img src="<?php echo $url; ?>" loading="lazy" alt="" class="icon icon-big">

          <div class="category-text category-text-2"><strong><?php echo $item['single_cursos_painel_txt']; ?></strong></div>

        </div>

        <?php endforeach; else: echo ""; endif;?>

        </div>
      </div>
    </div>
  </div>

  <div class="block-course">
    <div class="text-medium"><?php if($sobre_desc): echo $sobre_desc; else: echo "Não possui descrição"; endif; ?> </div>
  </div>

  <div class="block-course">
    <h4 class="block-course-title"><?php echo $prof_title; ?></h4>
    <?php if($prof_title): foreach($prof_item as $item): ?>  
    
        <?php foreach($item['single_curos_prof_img'] as $img): ?>  
        <?php $url = wp_get_attachment_url($img, 'full', '', array('class' => '')); ?>  
        <?php endforeach; ?>
              
    <div class="quote-card-body"><img src="<?php echo $url; ?>" alt="" class="avatar-2">
      <div class="quote-card-text">
        <h4 class="quote-card-heading"><?php echo $item['single_cursos_prof_nome']; ?></h4>
        <div class="text-medium"><?php echo $item['single_cursos_prof_uni']; ?></div>
      </div>
    </div>
    <div class="text-medium cursos_desc_block"><?php echo $item['single_cursos_prof_desc']; ?></div>
    <?php endforeach;  else: echo "Ainda não possui professores"; endif;?>
    <div class="divider divider-course"></div>
  </div>
  <div class="block-course">

  <?php if($Modulo): foreach($Modulo as $model): ?>

    <div class="modulos">
      <h4 class="block-course-title"><?php echo $model['single_cursos_modulo_model']; ?></h4>

      <?php foreach($model['single_cursos_modulos_item'] as $parte): ?>

      <div class="boxed boxed-slim">
        <div class="title-modulo">
          <div class="number">
            <div class="text-block"><?php echo $parte['single_cursos_modulo_state']; ?></div>
          </div>
          <div class="title-modulo-text">
            <div><strong><?php echo $parte['single_cursos_modulo_tema']; ?></strong></div>
          </div>
        </div>
        <ul role="list" class="list-vertical list-vertical-slim">

        <?php echo $parte['single_cursos_modulo_desc']; ?>
      
        </ul>
      </div>
      
      <?php endforeach; ?>
    
    </div>

  <?php endforeach; endif; ?>

    <div class="divider divider-course"></div>
  </div>
  <div>
    <h4 class="display-heading-2 cta-center"><?php if($title_rev): echo $title_rev; else: echo ""; endif; ?></h4>
    <div class="text-medium center"><?php if($cta_desc): echo $cta_desc; else: echo ""; endif; ?></div>
    <a href="<?php if($cta_link): echo $cta_link; else: echo ""; endif; ?>" style="background:<?php echo $cta_color; ?>" target="_blank" class="hero__btn max_450 w-button"><?php if($cta_title): echo $cta_title; else: echo "Tenho Interesse"; endif; ?></a>
  </div>
</div>


</div>
</div>
  <div class="bg square-2"></div>
  <div class="course-bg-wrapper">
    <div class="bg shape-1 shape-3"></div>
  </div>
  </div>

    <?php endwhile; wp_reset_postdata();?>

  <div class="section">
    <div class="main-container main-container-small">
      <div class="container-large align-center section-title">
        <h4 class="large-heading"><?php echo $aluno_title; ?></h4>
      </div>
      <div class="w-layout-grid quotes-grid-thirds">
      
      <?php if($aluno_bloco): foreach($aluno_bloco as $bloco): ?>

        <?php foreach($bloco['alunos_single_cursos_blocos_icon'] as $img): ?>
            <?php $url = wp_get_attachment_image_url($img,'full','',array('class'=>'')); ?>
          <?php endforeach; ?>

        <div class="boxed small-quote-box shadow"><img src="<?php echo $url; ?>" alt="" class="avatar avatar-small quote-box-avatar">
          <div class="text-depoimentos">“<?php echo $bloco['alunos_single_cursos_blocos_desc']; ?>”</div>
          <div class="text-small-2 quote-author"><?php echo $bloco['alunos_single_cursos_blocos_name']; ?>, <?php echo $bloco['alunos_single_cursos_blocos_region']; ?></div>
        </div>

        <?php endforeach; endif;?>

      </div>
    </div>
  </div>


    <?php get_template_part('templates/content/loop','cursos');?>
    <?php get_template_part('templates/html','footer');?>