<?php get_template_part('templates/html','header');?>


<?php     
  
    //while (have_posts()) : the_post(); $loop[] = get_the_ID();
    $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
    $nday = get_post_meta(get_the_ID(), 'component_evento_nday', true);
    $month = get_post_meta(get_the_ID(), 'component_evento_month', true);
    $cdesc = get_post_meta(get_the_ID(), 'component_evento_desc', true);
    $hour = get_post_meta(get_the_ID(), 'component_evento_hour', true);
    $city = get_post_meta(get_the_ID(), 'component_evento_city', true);
    $itens_event = get_post_meta(get_the_ID(), 'single_event_painel_itens', true);
    $cta = get_post_meta(get_the_ID(), 'component_evento_cta', true);
    $cta_link = get_post_meta(get_the_ID(), 'component_evento_cta_link', true);

    //video
    $video_thumb = get_post_meta(get_the_ID(), 'single_eventos_video_thumb', true);
    $thumb = wp_get_attachment_image_src($video_thumb, 'medium_large');
    $video_link = get_post_meta(get_the_ID(), 'single_eventos_video_link', true);

    // agenda do evento
    $agenda_title = get_post_meta(get_the_ID(), 'single_eventos_agenda_title', true);
    $agenda_desc = get_post_meta(get_the_ID(), 'single_eventos_agenda_desc', true);


    // participantes

    $participant_title = get_post_meta(get_the_ID(), 'single_eventos_agenda_title', true);
    $participant_itens = get_post_meta(get_the_ID(), 'single_eventos_participantes_itens', true);

    // Participe da Revolução
    
    $revo_title = get_post_meta(get_the_ID(), 'single_eventos_revolucao_title', true);
    $revo_desc = get_post_meta(get_the_ID(), 'single_eventos_revolucao_desc', true);
    $revo_cta = get_post_meta(get_the_ID(), 'single_eventos_revolucao_cta', true);
    $revo_link = get_post_meta(get_the_ID(), 'single_eventos_revolucao_link', true);
    $revo_color = get_post_meta(get_the_ID(), 'single_eventos_revolucao_color', true);

    // Depoimentos
    $aluno_title = get_post_meta(get_the_ID(), 'alunos_single_eventos_title', true);
    $aluno_bloco = get_post_meta(get_the_ID(), 'alunos_single_eventos_blocos', true);


  ?>

<div class="section hero">
  <div class="main-container">
    <div class="course-wrapper">
      <div class="features">
        <div class="header-course">
          <div class="details-event">
            <div class="infos-event">
              <div class="text-pill pill-white">
                <h6 class="heading-pill">Evento</h6>
              </div>
              <div class="category-event"><img src="<?php echo get_template_directory_uri(); ?>/_src/images/relogio.svg" loading="lazy" alt="" class="icon-small icon-hour">
                <div class="text-icons"><?php if($hour): echo $hour; else: echo ""; endif; ?> horas</div><img src="<?php echo get_template_directory_uri(); ?>/_src/images/location.svg" loading="lazy" alt="" class="icon-small icon-local">
                <div class="text-icons"><?php if($city): echo $city; else: echo ""; endif; ?></div>
              </div>
              <h4 class="display-heading-blue"><?php the_title(); ?></h4>
              <div class="text-medium"><?php echo the_content(); ?></div>
              <a href="<?php if($cta): echo $cta; else: echo ""; endif; ?>" target="_blank" class="hero__btn max_450 w-button"><?php if($city_link): echo $city_link; else: echo "Participar"; endif; ?></a>
            </div>
          </div>
        </div>

        <div class="video-wrapper shadow-large space-top space-bottom">
          <img src="<?php if($thumb[0]): echo $thumb[0]; else: echo get_template_directory_uri().'/assets/images/single-event.jpg'; endif;?>" alt="" class="">
          <a class="various fancybox video-button w-inline-block" href="<?php if($video_link): echo 'https://www.youtube.com/watch?v='.$video_link; else: echo get_template_directory_uri().'/assets/images/single-event.jpg'; endif; ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-play.svg" alt="" class="image-3">
          </a>
        </div>

        <div class="card-course-curso">
        <div class="boxed boxed-course">
          <div class="image-link-box-content">
            <div class="date-event">
              <div class="day"><?php echo $nday; ?></div>
              <div class="month"><?php echo $month; ?></div>
            </div>
            <div class="description">
              <p class="paragraph"><?php echo $cdesc; ?></p>
            </div>
            <a href="http://mo.na/sign-up" target="_blank" class="hero__btn max_100 buy__btn w-button">Tenho interesse</a>
            <div class="divider divider-card-course"></div>
            <div class="details details-interna">

            <?php if($itens_event): foreach($itens_event as $item): ?>
              <?php foreach($item['single_event_painel_icon'] as $img): ?>
                <?php $url =  wp_get_attachment_url($img, 'full', '', array('class' => '')); ?>
              <?php endforeach; ?>

              <div class="category category-100 card-course-curso__div">
                <img src="<?= $url; ?>" loading="lazy" alt="" class="icon icon-big">
                <div class="category-text category-text-2"><strong><?= $item['single_event_painel_txt']; ?></strong></div>
              </div>
              
            <?php endforeach; endif; ?>

            </div>
          </div>
        </div>
      </div>

        <div class="block-course">
          <h4 class="block-course-title"><?php if($agenda_title): echo $agenda_title; else: echo "agenda do evento"; endif; ?></h4>
          <div class="text-medium"> <?php if($agenda_desc): echo $agenda_desc; else: echo "Sem Descrição"; endif; ?> </div>
          <a href="http://mo.na/sign-up" target="_blank" class="hero__btn max_450 w-button">PARTICIPAR GRATUITAMENTE</a>
          <div class="divider divider-course"></div>
        </div>
        <div class="block-course">
        
          <h4 class="block-course-title"><?php if($participant_title): echo $participant_title; else: echo "Participantes"; endif; ?></h4>

          <?php if($participant_itens): foreach($participant_itens as $item): ?>
            <?php foreach($item['single_eventos_participantes_thumb'] as $img): ?>
              <?php $url =  wp_get_attachment_url($img, 'full', '', array('class' => '')); ?>
            <?php endforeach; ?>

          <div class="quote-card-body"><img src="<?php echo $url; ?>" alt="" class="avatar-2">
            <div class="quote-card-text">
              <h4 class="quote-card-heading"><?php echo $item['single_eventos_participantes_name']; ?></h4>
              <div class="text-medium"><?php echo $item['single_eventos_participantes_uni']; ?></div>
            </div>
          </div>

          <div class="text-medium"> <?php echo $item['single_eventos_participantes_desc']; ?> </div>
          
          <?php endforeach; endif; ?>
          
          <a href="http://mo.na/sign-up" target="_blank" class="hero__btn max_450 w-button">PARTICIPAR GRATUITAMENTE</a>
          <div class="divider divider-course"></div>
        </div>
        <div>
          <h4 class="display-heading-2 cta-center"><?php if($revo_title): echo $revo_title; else: echo ""; endif;?></h4>
          <div class="text-medium center"><?php if($revo_desc): echo $revo_desc; else: echo "Não possui descrição"; endif; ?></div>
          <a href="http://mo.na/sign-up" style="background:<?php echo $revo_color; ?>;" target="_blank" class="hero__btn max_450 w-button"><?php if($revo_cta): echo $revo_cta; else: echo "Tenho Interesse"; endif; ?></a>
        </div>
      </div>

      
    </div>
  </div>
  <div class="bg square-2 square-3"></div>
  <div class="course-bg-wrapper">
    <div class="bg shape-1 shape-3 shape-4"></div>
    <div class="bg shape-2 shape-2-left"></div>
  </div>
</div>

<div class="section">
    <div class="main-container main-container-small">
      <div class="container-large align-center section-title">
        <h4 class="large-heading"><?php echo $aluno_title; ?></h4>
      </div>
      <div class="w-layout-grid quotes-grid-thirds">
      
      <?php if($aluno_bloco): foreach($aluno_bloco as $bloco): ?>

        <?php foreach($bloco['alunos_single_eventos_blocos_icon'] as $img): ?>
            <?php $url = wp_get_attachment_image_url($img,'full','',array('class'=>'')); ?>
          <?php endforeach; ?>

        <div class="boxed small-quote-box shadow"><img src="<?php echo $url; ?>" alt="" class="avatar avatar-small quote-box-avatar">
          <div class="text-depoimentos">“<?php echo $bloco['alunos_single_eventos_blocos_desc']; ?>”</div>
          <div class="text-small-2 quote-author"><?php echo $bloco['alunos_single_eventos_blocos_name']; ?>, <?php echo $bloco['alunos_single_eventos_blocos_region']; ?></div>
        </div>

        <?php endforeach; endif;?>

      </div>
    </div>
  </div>

<?php get_template_part('templates/frontpage','destaque'); ?>
<?php get_template_part('templates/html','footer'); ?>